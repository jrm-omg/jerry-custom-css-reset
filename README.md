# Jerry's custom CSS reset

Trying to make the CSS reset great again

- [CSS reset for webpage](reset-webpage.css)
- [CSS reset for SPA (Single Page Application)](reset-spa.css)

Both files are exactly the same, excepting the body's `height` (SPA) that becomes `min-height` (webpage)

## Inspired by

- https://www.joshwcomeau.com/css/custom-css-reset/
- https://github.com/jensimmons/cssremedy/blob/master/css/remedy.css
- https://cdn.jsdelivr.net/npm/modern-normalize/modern-normalize.css
- https://codepen.io/absolutholz/post/html-and-body-element-height-100
- https://caniuse.com/mdn-css_types_length_viewport_percentage_units_dynamic
- https://github.com/primer/css/blob/main/src/support/variables/typography.scss
